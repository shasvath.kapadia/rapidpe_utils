#!/usr/bin/env python

import os
import re
import argparse

from glue.ligolw import ligolw, utils, lsctables
lsctables.use_in(ligolw.LIGOLWContentHandler)
from glue.ligolw.utils import ligolw_add
from glue.lal import Cache, CacheEntry

from lalinference.rapid_pe import common_cl, lalsimutils

import overlaplib

# FIXME: Send to common_cl
VALID_INTR_PARAMS = ("mass1", "mass2", "mchirp", "eta", "spin1z", "spin2z")
def parse_intrinsic_params(args):
    pt_dict = {}
    for p in VALID_INTR_PARAMS:
        if hasattr(args, p) and getattr(args, p) is not None:
            pt_dict[p] = getattr(args, p)

    # FIXME: Double counting...
    if "mchirp" in pt_dict and "mass1" in pt_dict:
        del pt_dict["mass1"]
        del pt_dict["mass2"]

    if len(pt_dict) == 0:
        raise ArgumentError("No valid intrinsic parameters were supplied to the argument parser.")

    return pt_dict

def parse_intrinsic_params_from_coinc(coincf):
    xmldoc = utils.load_filename(coincf, contenthandler=ligolw.LIGOLWContentHandler)
    sngl_row = lsctables.SnglInspiralTable.get_table(xmldoc)[0]

    pt_dict = {}
    for p in VALID_INTR_PARAMS:
        if hasattr(sngl_row, p):
            pt_dict[p] = getattr(sngl_row, p)

    # FIXME: Double counting...
    if "mchirp" in pt_dict and "mass1" in pt_dict:
        del pt_dict["mass1"]
        del pt_dict["mass2"]

    if len(pt_dict) == 0:
        raise ArgumentError("Coinc file does not contain any valid intrinsic parameters.")

    return pt_dict

def get_tmplt_id_from_full_bank(intr_pt, bank_file, verbose=False):
    """
    Obtain the index of the closest point to the intrinsic point given. Note that this requires a full load of the bank, construction of a BallTree and the subsequent query. It's very slow.
    """
    tmplt_bank = overlaplib.read_bank(bank_file)
    tmplt_ids = overlaplib.bank_to_ids(tmplt_bank)
    tmplt_bank = overlaplib.bank_to_pts(tmplt_bank, intr_pt.keys())
    idx = overlaplib.find_pt_inexact(intr_pt.values(), tmplt_bank)[0]
    if verbose:
        print "Requested point: %s" % str(intr_pt)
        print "Located point: %s" % str(dict(zip(intr_pt.keys(), tmplt_bank[idx])))

    return tmplt_ids[idx]

def map_bank_to_overlap_file(bankf, subbank_files):
    # Map to the overlap file via ID
    bank_id = re.search("([0-9]{4})", bankf).group(1)
    bank_id = "_" + str(int(bank_id)) + "_"
    split_bank_overlap_file = filter(lambda b: bank_id in b, subbank_files)

    if len(split_bank_overlap_file) == 0:
        raise ValueError("Bank ID does not map to an overlap file.")
    elif len(split_bank_overlap_file) > 1:
        raise ValueError("Bank ID maps to multiple overlap files.")

    return split_bank_overlap_file[0]

argp = argparse.ArgumentParser()
argp.add_argument("--template-id", type=int, help="Use this integer as the template ID to identify the correct template.")
argp.add_argument("--coinc-file", help="Use information from this coinc file.")
argp.add_argument("--bank-file", help="Full bank XML location. Required.")
argp.add_argument("--bank-file-cache", help="Split bank XML cache file. Required.")
argp.add_argument("--bank-overlap-cache", help="Split bank overlap cache file. Required.")
argp.add_argument("--bank-strategy", default='single', help="Strategy to deal with an intrinsic point who's support spans multiple banks. Valid choices are 'single' (Use only the bank the point appears in), 'cache' (create a cache file for use with the gridder), and 'coalesce' (do a full merge of the identified banks and output this).")
argp.add_argument("--coalesce-banks", help="Parameters to this argument specifies the root stem of the filenames to write. These paths are returned instead of the pre-existing bank.")
argp.add_argument("--verbose", action="store_true", help="Be verbose.")
common_cl.add_intrinsic_params(argp)
args = argp.parse_args()

bank_strategy = args.bank_strategy
VALID_STRATEGIES = ("single", "cache", "coalesce")
if bank_strategy not in VALID_STRATEGIES:
    raise ValueError("Invalid --bank-strategy, must be one of %s" % ", ".join(VALID_STRATEGIES))
coalesce_banks = args.coalesce_banks

#
# Generate subbank mapping: we're guaranteed to need this
#

# Since this can take a long time to do each time, and all we want is the IDs,
# we cache that mapping for faster access
import json
SUBBANK_MAP_CACHE = "/home/pankow/rapidpe/etc/.subbank_mapping_cache"
if os.path.exists(SUBBANK_MAP_CACHE):
    with open(SUBBANK_MAP_CACHE) as fin:
        subbank_mapping = json.load(fin)
else:
    split_bank_files = [ce.path for ce in Cache.fromfile(open(args.bank_file_cache))]
    subbank_mapping = {}
    for split_bank in split_bank_files:
        subbank_mapping.update(overlaplib.map_subbank(split_bank))
    with open(SUBBANK_MAP_CACHE, "w") as fout:
        json.dump(subbank_mapping, fout)

overlap_files = [ce.path for ce in Cache.fromfile(open(args.bank_overlap_cache))]

if args.template_id is not None:
    # We just need to map into subbanks... don't load the whole bank
    pass
elif args.coinc_file is not None:
    intr_pt = parse_intrinsic_params_from_coinc(args.coinc_file)
else:
    intr_pt = parse_intrinsic_params(args)

# Identify the template ID we'll be using
if args.template_id is not None:
    tmplt_id = args.template_id
else:
    tmplt_id = get_tmplt_id_from_full_bank(intr_pt, args.bank_file, verbose=args.verbose)

bank_list, olap_list = [], []

# Get the subbanks which contain this ID
if coalesce_banks:
    subbanks = subbank_mapping.keys()
else:
    subbanks = overlaplib.map_tmplt_to_subbank(tmplt_id, subbank_mapping)

for bankf in subbanks:
    split_bank_overlap_file = map_bank_to_overlap_file(bankf, overlap_files)

    # Check if the id is present
    id1, id2 = overlaplib.load_overlap_id_data(split_bank_overlap_file, verbose=args.verbose)
    if bank_strategy != "single" and any(id2 == tmplt_id):
        bank_list.append(bankf)
        olap_list.append(split_bank_overlap_file)

    if not any(id1 == tmplt_id):
        continue

    if bank_strategy == "single":
       break

if False:
    print "Will coalesce %d, %d subbanks" % (len(olap_list), len(bank_list))

if bank_strategy == "coalesce":
    bankf = coalesce_banks + ".xml.gz"
    split_bank_overlap_file = coalesce_banks + ".hdf"

    xmldoc = utils.load_filename(bank_list[0], contenthandler=ligolw.LIGOLWContentHandler)
    xmldoc = ligolw_add.ligolw_add(xmldoc, bank_list[1:])
    utils.write_filename(xmldoc, bankf, gz=True)
    overlaplib.concatenate_overlaps(olap_list, split_bank_overlap_file)

elif bank_strategy == "cache":
    split_bank_overlap_file = coalesce_banks + ".cache"
    from glue.lal import CacheEntry
    with open(split_bank_overlap_file, "w") as fout:
        for bank in olap_list:
            ce = CacheEntry(None, None, None, "file://localhost%s" % bank)
            print >>fout, str(ce)
 
print os.path.realpath(bankf), os.path.realpath(split_bank_overlap_file)
