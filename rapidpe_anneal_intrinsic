#!/usr/bin/env python
import os, copy, sys

import numpy
import scipy.optimize

import lalburst, lalsimulation, lal, lalinspiral
from lalinference.rapid_pe import lalsimutils, amrlib

from glue.ligolw import utils, lsctables, ligolw, ilwd
lsctables.use_in(ligolw.LIGOLWContentHandler)
from glue.ligolw.utils import process
from pylal.series import read_psd_xmldoc, LIGOLWContentHandler

def write_to_xml(cells, intr_prms, fvals=None, fname=None, verbose=False):
    """
    Write a set of cells, with dimensions corresponding to intr_prms to an XML file as sim_inspiral rows.
    """
    xmldoc = ligolw.Document()
    xmldoc.appendChild(ligolw.LIGO_LW())
    procrow = process.append_process(xmldoc, program=sys.argv[0])
    procid = procrow.process_id
    process.append_process_params(xmldoc, procrow, process.process_params_from_dict({}))

    rows = ["simulation_id", "process_id", "numrel_data"] + list(intr_prms)
    if fvals is not None:
        rows.append("alpha1")
    sim_insp_tbl = lsctables.New(lsctables.SimInspiralTable, rows)
    for itr, intr_prm in enumerate(cells):
        sim_insp = sim_insp_tbl.RowType()
        # FIXME: Need better IDs
        sim_insp.numrel_data = "INTR_SET_%d" % itr
        sim_insp.simulation_id = ilwd.ilwdchar("sim_inspiral:sim_inspiral_id:%d" % itr)
        sim_insp.process_id = procid
        if fvals:
            sim_insp.alpha1 = fvals[itr]
        for p, v in zip(intr_prms, intr_prm._center):
            setattr(sim_insp, p, v)
        sim_insp_tbl.append(sim_insp)

    xmldoc.childNodes[0].appendChild(sim_insp_tbl)
    if fname is None:
        channel_name = ["H=H", "L=L"]
        ifos = "".join([o.split("=")[0][0] for o in channel_name])
        #start = int(event_time)
        start = 0
        fname = "%s-MASS_POINTS-%d-1.xml.gz" % (ifos, start)
    utils.write_filename(xmldoc, fname, gz=True, verbose=verbose)

# FIXME: copied from script
def determine_region(pt, pts, ovrlp, ovrlp_thresh):
    """
    Given a point (pt) in a set of points (pts), with a function value at those points (ovrlp), return a rectangular hull such that the function exceeds the value ovrlp_thresh.
    """
    import bisect
    sidx = bisect.bisect(ovrlp, ovrlp_thresh)
    print "Found %d neighbors with overlap >= %f" % (len(ovrlp[sidx:]), ovrlp_thresh)

    cell = amrlib.Cell.make_cell_from_boundaries(pt, pts[sidx:], symmetric=False)
    return cell, sidx

def chi(m1, m2, s1, s2):
    return (m1 * s1 + m2 * s2) / (m1 + m2)

def is_in(pt, cell):
    for p, (lb, ub) in zip(pt, cell._bounds):
        if p < lb or p > ub:
            return False
    return True

intr_prms = ("mass1", "mass2")

psd_xmldoc = utils.load_filename("psd.xml.gz", contenthandler=LIGOLWContentHandler)
psd = read_psd_xmldoc(psd_xmldoc)["H1"]

#psd = FrequencySeries(psd.data, psd.deltaF)
#freq = psd.sample_frequencies
freq = numpy.linspace(0, psd.deltaF*len(psd.data), len(psd.data))

# Chose our new FD spacing
#new_delta_f = 0.125/4
new_delta_f = 0.125
new_f = numpy.arange(0, 4096, new_delta_f)

# Check for crazy values
#print numpy.interp(new_f, freq, numpy.abs(psd.data))
new_psd = lal.CreateREAL8FrequencySeries("PSD", lal.LIGOTimeGPS(0.), 0., new_delta_f, lal.HertzUnit, len(psd.data))
new_psd.data.data = psd.data
psd = new_psd

f_low_integrand, f_high_integrand = 30, 1000

# Overlap object, the integration starts at 40 Hz, goes to 1000 Hz, and accomodates PSDs up
# to a Nyquist of 2048, with a spacing of new_delta_f, and the input PSD from last cell.
olap = lalsimutils.Overlap(f_low_integrand, f_high_integrand, 2048, new_delta_f, psd, False)

# Count the injections present
fname = "HL-INJECTIONS_0-1000000000-1000.xml.gz"
"""
inj = lalinspiral.SimInspiralTableFromLIGOLw(fname, 1000000000, 1000001000)
cntr = 0
m1_dist, m2_dist, chi = [], [], []
while inj.next is not None:
    cntr += 1
    m1_dist.append(max(inj.mass1, inj.mass2))
    m2_dist.append(min(inj.mass1, inj.mass2))
    chi.append(inj.mass1 * inj.spin1z + inj.mass2 * inj.spin2z)
    chi[-1] /= (inj.mass1 + inj.mass2)
    inj = inj.next
print "Loaded %d injections" % cntr
"""

min_mass1, min_mass2, max_mass1, max_mass2 = 5.0, 5.0, 50.0, 50.0

def unpack(x_in, *args):
    x, y = max(x_in), min(x_in)
    # the 'anneal' function seems to ignore its bounds,
    # so this prevents it from going too far out
    if x > max_mass1 or x < min_mass1:
        return 1e10
    if y > max_mass2 or y < min_mass2:
        return 1e10
    try:
        olap = calc_overlap(x, y)
    except AssertionError:
        # There is a possibility that, despite all efforts to the contrary, that the df is still
        # not sufficient. If so, ignore it for now.
        print "Warrning, df insufficient for waveform %f %f" % (x, y)
        return 1e10
    #print x, y, olap
    evals[tuple(x_in)] = 1-olap
    return evals[tuple(x_in)]

inj = lalinspiral.SimInspiralTableFromLIGOLw(fname, 1000000000, 1000001000)
print inj

# Which waveform family are we working with?
#wfm_family = "TaylorF2"
# This doesn't always work --- memory error...
#wfm_family = "IMRPhenomP"
wfm_family = "IMRPhenomD"

inj_pts, inside, outside = [], [], []
#min_i, max_i = map(int, sys.argv[1:])
i = 0
while inj.next is not None:

    # Override with new values
    inj.mass1 = 32.1
    inj.mass2 = 14.6

    # Explcitly zero spins for now
    inj.spin1z, inj.spin2z = 0, 0
    # symmetrize masses
    inj.mass1, inj.mass2 = max(inj.mass1, inj.mass2), min(inj.mass1, inj.mass2)

    # What's our input intrinsic parameter set?
    print inj.mass1, inj.mass2, inj.spin1x, inj.spin1y, inj.spin1z, inj.spin2x, inj.spin2y, inj.spin2z

    # Generate it to make sure the WF generator doesn't fail
    hp, hx = lalinspiral.SimInspiralChooseWaveformFromSimInspiral(inj, 1.0/4096)

    # Convert the sim inspiral to a sngl inspiral because that's what the generic overlap routines
    # in lalinference.rapid_pe.lalsimutils assume
    # FIXME: I need to update that routine to optionally use a sim
    inj_row = lsctables.SnglInspiral()
    for snglattr in lsctables.SnglInspiralTable.validcolumns.keys():
        try:
            setattr(inj_row, snglattr, getattr(inj, snglattr))  
            #print snglattr, getattr(inj_row, snglattr)
        except AttributeError:
            pass
    
    inj_copy = copy.copy(inj_row)

    # Thin wrapper over lalsimutils.overlap, this allows us to simply change the input intrinsic
    # parameters and avoid remaking the injection row every time.
    def calc_overlap(m1, m2):
        inj_copy.mass1, inj_copy.mass2 = m1, m2
        o, inj_norm, _ = lalsimutils.overlap(inj_row, inj_copy, olap, \
                                             new_delta_f, 40, wfm_family, wfm_family)
        return o

    # Test to make sure overlap returns unity for identical injections
    print calc_overlap(inj_row.mass1, inj_row.mass2)

    # Initial guess
    guess = numpy.array([5.0, 5.0])

    # Save the overlap evaluations
    evals = dict()

    res = scipy.optimize.minimize(unpack, guess)
    print res
    guess = res.x

    fxf_min = 1.0
    status, ntries = -1, 10
    while status != 1 and ntries > 0:
        # NOTE: the "lower" and "upper" bounds here are not global bounds.
        # They are bounds on the allowed jumps in the parameters, see:
        # https://github.com/scipy/scipy/issues/1653
        # It's highly unlikely this will be fixed as anneal is deprecated.
        xf, fxf, tf, feval, iters, accept, status = scipy.optimize.anneal(func=unpack, x0=guess, \
                                                                    lower = (min_mass1, min_mass2),
                                                                    upper = (max_mass1, max_mass2),
                                                                    full_output=True)

        print "Found minimum (%f) at %s, with final temperature %f, after %d evaluations and %d temperature adjustments. %d points were accepted." %  \
        (fxf, str(xf), tf, feval, iters, accept)
        if fxf_min > fxf:
            guess = xf
            fxf_min = fxf
        ntries -= 1
    
    # Retrieve all of our overlap evaluations
    #xy = numpy.array([(max(k), min(k)) for k in evals.keys()]) 
    xy = numpy.array([(max(k), min(k),
        -0.95 if numpy.random.uniform() < 0.5 else 0.95,
        -0.95 if numpy.random.uniform() < 0.5 else 0.95 ) for k in evals.keys()]) 
    print xy
    zeval = 1-numpy.array([evals[k] for k in evals.keys()])

    #intr_prms = ("mass1", "mass2")
    intr_prms = ("mass1", "mass2", "spin1z", "spin2z")

    # Points to use in the next set of calculations -- appropriately transformed
    xy_tau0tau3 = amrlib.apply_transform(numpy.array(xy, copy=True), intr_prms, "tau0_tau3")
    sort_order = zeval.argsort()
    xy_tau0tau3 = xy_tau0tau3[sort_order]
    zeval = zeval[sort_order]
    
    t0t3_in = numpy.array([inj_row.mass1, inj_row.mass2, 0, 0], copy=True)
    #t0t3_in = numpy.array([inj_row.mass1, inj_row.mass2], copy=True)
    t0t3_in = amrlib.apply_transform(t0t3_in[numpy.newaxis,:], intr_prms, "tau0_tau3")[0]
   
    overlap_thresh = 0.90
    init_region, sidx = determine_region(None, xy_tau0tau3, zeval, overlap_thresh)

    # If we don't have an init_region after this, it means we have *no* points with appreciable
    # overlap. Deal with it.
    
    #print init_region._center, init_region._bounds

    print t0t3_in, init_region._bounds

    extent_str = " ".join("(%f, %f)" % bnd for bnd in map(tuple, init_region._bounds))
    center_str = " ".join(map(str, init_region._center))
    print "Created cell with center " + center_str + " and extent " + extent_str

    grid, spacing = amrlib.create_regular_grid_from_cell(init_region, side_pts=5, return_cells=True)

    grid, spacing = amrlib.refine_regular_grid(grid, spacing, return_cntr=True)
    print "%d cells after refinement" % len(grid)
    grid = amrlib.prune_duplicate_pts(grid, init_region._bounds, spacing)

    grid = numpy.array(grid)
    bounds_mask = amrlib.check_grid(grid, intr_prms, "tau0_tau3")
    grid = grid[bounds_mask]
    print "%d cells after bounds checking" % len(grid)

    if len(grid) == 0:
        exit("All cells would be removed by physical boundaries.")

    # Convert back to physical mass
    grid = amrlib.apply_inv_transform(grid, intr_prms, "tau0_tau3")

    cells = amrlib.grid_to_cells(grid, spacing)

    npy_grid = amrlib.pack_grid_cells(cells, spacing)
    # FIXME: specify level better
    amrlib.serialize_grid_cells({1: npy_grid}, "G197392_grid")

    fname = "HL-MASS_POINTS_LEVEL_0-0-1.xml.gz"
    write_to_xml(cells, intr_prms, None, fname, verbose=True)
    break

numpy.save("initial_region", init_region._bounds)
